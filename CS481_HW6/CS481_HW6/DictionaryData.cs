﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CS481_HW6
{

    //set a dicationary data which are Definitions[type, definition,example,image url and emoji], word, and pronunciation. 
    public partial class DictionaryData
    {
        [JsonProperty("definitions")]
        public Definition[] Definitions { get; set; }

        [JsonProperty("word")]
        public string Word { get; set; }

        [JsonProperty("pronunciation")]
        public object Pronunciation { get; set; }
    }

    //Definitions[type, definition, example, image url and emoji]
    public partial class Definition
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string DefinitionDefinition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }

        [JsonProperty("image_url")]
        public object ImageUrl { get; set; }

        [JsonProperty("emoji")]
        public object Emoji { get; set; }
    }


    //Dictionary class is converted. 
    public partial class Dictionary
    {
        public static Dictionary FromJson(string json) => JsonConvert.DeserializeObject<Dictionary>(json, CS481_HW6.Converter.Settings);
    }


    //Serialize data. 
    public static class Serialize
    {
        public static string ToJson(this Dictionary self) => JsonConvert.SerializeObject(self, CS481_HW6.Converter.Settings);
    }


    //Converte the Json data.
    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }



}