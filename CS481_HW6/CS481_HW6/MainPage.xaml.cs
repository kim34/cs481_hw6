﻿using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;



namespace CS481_HW6
{
    public partial class MainPage : ContentPage
    {
        
        Service restService;

        public MainPage()
        {
            InitializeComponent();
            restService = new Service();
        }


        // get a website and the API key. 
        public static class Info
        {
            public const string DictionaryWeb = "https://owlbot.info/api/v2/dictionary/";
            public const string DictionaryAPIKey = "f8d38ebfddeb228a346bf720b7d98cc3028f11ab";
        }

        //button event, If the clicked the button, web server is requested.
        async void OnButtonClicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(WordEntry.Text))
            {
                DictionaryData disctionaryData = await restService.GetWeatherDataAsync(GenerateRequestUri(Info.DictionaryWeb));
                BindingContext = disctionaryData;
            }
        }

        //request web databse
        string GenerateRequestUri(string Point)
        {
            string Uri = Point;
            Uri += $"?q={WordEntry.Text}";
            Uri += $"&APPID={Info.DictionaryAPIKey}";

            return Uri;
        }
    }
}