﻿using System;
using System.Diagnostics;
using System.Net.Http;      //required HTTP.
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;

namespace CS481_HW6
{
    public class Service
    {
        
        //Get a sources from the HTTP. If success respond, get a source, otherwise give an error. 
        public async Task<DictionaryData> GetWeatherDataAsync(string uri)
        {
            HttpClient client;
            client = new HttpClient();
            DictionaryData dictionaryData = null;


            // request source
            try
            {
                HttpResponseMessage response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    dictionaryData = JsonConvert.DeserializeObject<DictionaryData>(content);
                }
            }

            // fail to get a source. 
            catch (Exception without)
            {
                Debug.WriteLine("{0}", without.Message);
            }

            return dictionaryData;
        }
    }
}
